FROM centos:latest
MAINTAINER Jakub Veverka <veverka.kuba@gmail.com>

# update
RUN yum -y update

# install base packages
RUN yum -y install openssh-server sudo openssh-clients which 
RUN ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key && ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key && sed -i "s/#UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config && sed -i "s/UsePAM.*/UsePAM no/g" /etc/ssh/sshd_config

RUN sed -i "s/\(.*pam_loginuid.so\)/#\1/g" /etc/pam.d/sshd
RUN sed -i "s/\(.*requiretty.*\)/#\1/g" /etc/sudoers

ADD docker/set_root_pw.sh /set_root_pw.sh
ADD docker/run.sh /run.sh
RUN chmod +x /*.sh

RUN curl -L https://opscode.com/chef/install.sh | bash

EXPOSE 22
#CMD ["/run.sh"]
